from setuptools import find_packages, setup
import os
from glob import glob

package_name = "oakd_bluerov"

setup(
    name=package_name,
    version="0.0.0",
    packages=find_packages(exclude=["test"]),
    data_files=[
        ("share/ament_index/resource_index/packages", ["resource/" + package_name]),
        ("share/" + package_name, ["package.xml"]),
        (
            os.path.join("share", package_name, "launch"),
            glob(os.path.join("launch", "*launch.[pxy][yma]*")),
        ),
        (
            os.path.join("share", package_name, "cfg"),
            glob(os.path.join("cfg", "*")),
        ),
    ],
    install_requires=["setuptools", "depthai>=2.26", "pyapriltags>=3.3"],
    zip_safe=True,
    maintainer="aaron",
    maintainer_email="aaron@todo.todo",
    description="TODO: Package description",
    license="BSD-3-Clause",
    tests_require=["pytest"],
    entry_points={
        "console_scripts": ["oakd_bluerov = oakd_bluerov.ros2_entrypoint:entrypoint"],
    },
)
