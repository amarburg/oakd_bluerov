ARG ROS_DISTRO=rolling
FROM ros:$ROS_DISTRO-ros-base as ci

ENV DEBIAN_FRONTEND=noninteractive

# WORKDIR /root/ws_blue
# COPY . src/blue

# Install apt packages needed for CI
RUN apt-get -q update \
    && apt-get -q -y upgrade \
    && apt-get -q install --no-install-recommends -y \
            clang \
            clang-format-14 \
            clang-tidy \
            clang-tools \
            git \
            gnupg \
            libvorbis-dev \
            libvpx-dev \
            libwebp-dev \
            libx264-dev \
            libx265-dev \
            libxml2-dev \
            lsb-release \
            python3-dev \
            python3-pip \
            python3-venv \
            software-properties-common \
            stow \
            sudo \
            wget \
    && apt-get autoremove -y \
    && apt-get clean -y \
    && rm -rf /var/lib/apt/lists/*

# Install FFmpeg from source for Raspi
WORKDIR /usr/local/src
RUN git clone --depth 1 -b pios/bookworm https://github.com/RPi-Distro/ffmpeg.git
WORKDIR /usr/local/src/ffmpeg/build
RUN ../configure --prefix=/usr/local/ \
            --enable-gpl \
            --enable-nonfree \
            --enable-libvorbis \
            --enable-libvpx \
            --enable-libx264 \
            --enable-libx265 \
            --enable-libxml2 \
            --enable-libwebp \
    && make -j4 \
    && make install


FROM ci as deploy

# Configure a new non-root user
#
# ros image now includes a user "ubuntu" at UID 1000
ARG USERNAME=ubuntu
ARG USER_UID=1000
ARG USER_GID=$USER_UID

RUN echo $USERNAME ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/$USERNAME \
    && chmod 0440 /etc/sudoers.d/$USERNAME \
    && usermod -a -G dialout $USERNAME \
    && echo "source /usr/share/bash-completion/completions/git" >> /home/$USERNAME/.bashrc

ENV DEBIAN_FRONTEND=noninteractive

# Switch to the non-root user for the rest of the installation
USER $USERNAME
ENV USER=$USERNAME

# Python in Ubuntu is now marked as a "Externally managed environment",
# Per best practice, create a venv for local python packages
#
# These two ENVs effectively "activate" the venv for subsequent calls to
# python/pip within this Dockerfile
WORKDIR /home/$USERNAME
ENV VIRTUAL_ENV=/home/$USERNAME/.venv/oakd_bluerov
# Venv needs to be established in an environment which already contains the ROS paths
RUN . /opt/ros/${ROS_DISTRO}/setup.sh && python3 -m venv --system-site-packages $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

RUN echo "source /opt/ros/${ROS_DISTRO}/setup.bash" >> /home/$USERNAME/.bashrc
RUN echo "source /$VIRTUAL_ENV/bin/activate" >> /home/$USERNAME/.bashrc

ENV USER_WORKSPACE=/home/$USERNAME/oakd_ws
WORKDIR $USER_WORKSPACE
COPY --chown=$USER_UID:$USER_GID . src/oakd_bluerov

# # Install the Python requirements that aren't available as rosdeps
RUN python3 -m pip install -r $USER_WORKSPACE/src/oakd_bluerov/requirements.txt

RUN sudo apt-get -q update \
    && rosdep update \
    && rosdep install -y --from-paths src --ignore-src --as-root=pip:false --rosdistro ${ROS_DISTRO} \
    && sudo apt-get autoremove -y \
    && sudo apt-get clean -y \
    && sudo rm -rf /var/lib/apt/lists/*

RUN . /opt/ros/${ROS_DISTRO}/setup.sh && colcon build
RUN echo "source /$USER_WORKSPACE/install/setup.bash" >> /home/$USERNAME/.bashrc

ENTRYPOINT ["/usr/bin/bash"]
