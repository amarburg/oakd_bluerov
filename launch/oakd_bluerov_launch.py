from launch import LaunchDescription
from launch_ros.actions import Node


def generate_launch_description():

    return LaunchDescription(
        [
            Node(
                package="oakd_bluerov",
                executable="oakd_bluerov",
                name="oakd_bluerov",
                output="screen",
                emulate_tty=True,
                parameters=[
                    {"color_fps": 30, 
                      "mono_fps": 10, 
                      "rtsp/host": "localhost",
                      "rpi_h264_hwaccel": True }
                ],
            )
        ]
    )
