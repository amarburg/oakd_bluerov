import subprocess as sp
from subprocess import TimeoutExpired


class RtspStream:
    def __init__(self, popen_obj ):
        self.proc = popen_obj

    def write(self, data):
        self.proc.stdin.write(data)

    def close(self):
        self.proc.terminate()
        try:
            # ros will escalate toe SIGKILL after 5 seconds, need to beat it...
            self.proc.wait(1)
        except TimeoutExpired:
            self.proc.kill()


class RtspStreamer:
    def __init__(self, rtsp_host, rtsp_port):
        self.rtsp_host = rtsp_host
        self.rtsp_port = rtsp_port

        self.loglevel = "warning"

    def stream_url(self, stream_name):
        return f"rtsp://{self.rtsp_host}:{self.rtsp_port}/{stream_name}"

    def color_streamer(self, color_fps):
        rtsp_url = self.stream_url("color")

        # "-re 1 "
        # "-stream_loop -1 "
        # "-f h265 "

        args = (
            "ffmpeg "
            f"-loglevel {self.loglevel} "
            "-i pipe:0 "
            "-f rtsp "
            "-rtsp_transport tcp "
            f"-framerate {str(color_fps)} "
            "-vcodec copy "
            "-v error "
            f"{rtsp_url}"
        ).split()

        print(f"Color streaming at {rtsp_url}")
        try:
            print(args)
            return RtspStream(sp.Popen(args, stdin=sp.PIPE))
        except Exception as exc:
            exit(f"Error: ffmpeg exception {exc}")

    def mono_streamer(self, stream_name, resolution, pi_hw = False):
        rtsp_url = self.stream_url(stream_name)

        if pi_hw:
            # use accelerated h264 encoder on Raspi (must be compiled into ffmpeg)
            output_codec =  ( # Convert gray8 to yuv420p before sending to HW accel
                             f"-f rawvideo -pix_fmt yuv420p -s {resolution} "  
                              "-f mpegts -vcodec h264_v4l2m2m " )
        else:
            output_codec = "-f mpegts -vcodec libx265 -preset veryfast -tune zerolatency -pix_fmt yuv420p "

        args = (
            "ffmpeg "
            f"-loglevel {self.loglevel} "
            "-re -stream_loop -1 "
            f"-f rawvideo -pix_fmt gray8 -s {resolution} -i pipe:0 "
            f"{output_codec}"
            f"-f rtsp -rtsp_transport udp {rtsp_url}"
        ).split()

        print(f"Mono streaming at {rtsp_url}")
        try:
            print(args)
            return RtspStream(sp.Popen(args, stdin=sp.PIPE))
        except Exception as exc:
            exit(f"Error: ffmpeg exception {exc}")
