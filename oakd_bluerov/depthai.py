import depthai as dai

DEFAULT_COLOR_FPS = 30
DEFAULT_MONO_FPS = 10

class Oakd:
    def __init__(self):
        self.pipeline = dai.Pipeline()

    def color_pipeline(self, fps=DEFAULT_COLOR_FPS, quality=80):
        ## Color camera
        colorCam = self.pipeline.create(dai.node.ColorCamera)
        colorCam.setResolution(dai.ColorCameraProperties.SensorResolution.THE_4_K)
        colorCam.setInterleaved(False)
        colorCam.setColorOrder(dai.ColorCameraProperties.ColorOrder.BGR)
        colorCam.setFps(fps)

        videnc = self.pipeline.create(dai.node.VideoEncoder)
        videnc.setDefaultProfilePreset(
            fps, dai.VideoEncoderProperties.Profile.H265_MAIN
        )
        videnc.setKeyframeFrequency(fps * 4)
        videnc.setQuality(quality)

        colorCam.video.link(videnc.input)

        videncOut = self.pipeline.create(dai.node.XLinkOut)
        videncOut.setStreamName("color_encoded")
        videnc.bitstream.link(videncOut.input)

        return colorCam

    def mono_pipeline(self, camera_name, fps=DEFAULT_MONO_FPS):
        #
        # Set up B&W cameras for raw output 1280x800
        #
        monoCam = self.pipeline.create(dai.node.MonoCamera)
        monoCam.setResolution(dai.MonoCameraProperties.SensorResolution.THE_800_P)
        monoCam.setFps(fps)
        monoCam.setCamera(camera_name)

        xoutCam = self.pipeline.create(dai.node.XLinkOut)
        xoutCam.setStreamName(camera_name)
        monoCam.out.link(xoutCam.input)

        return monoCam
