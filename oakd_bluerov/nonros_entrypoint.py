#!/usr/bin/env python3
#
# This is a non-ROS test program
#
# Assumes a RTSP server is at rtsp://rtsp_host:rtsp_port/
#
# I use [mediamtx](https://github.com/bluenviron/mediamtx)
#
# To play with minimal latency:
#
# ffplay -flags low_delay -rtsp_transport tcp -fflags nobuffer rtsp://localhost:8554/preview

import depthai as dai
import numpy as np

from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter

from oakd_bluerov.apriltags import ApriltagDetector
from oakd_bluerov.rtsp_streams import RtspStreamer


def entrypoint():
    # Parse command line arguments
    parser = ArgumentParser(formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        "-rip",
        "--rtsp-host",
        default="localhost",
        type=str,
        help="Host of the RTSP Server",
    )
    parser.add_argument(
        "-rp", "--rtsp-port", default=8554, type=int, help="Port of the RTSP Server"
    )
    parser.add_argument(
        "-wt",
        "--width",
        default=1920,
        type=int,
        help="Width of the video/preview size. In multiple of 32",
    )
    parser.add_argument(
        "-ht",
        "--height",
        default=1080,
        type=int,
        help="Height of the video/preview size. In multiple of 8",
    )
    parser.add_argument(
        "--quality", "-q", default=100, type=int, help="Video quality, from 1 to 100"
    )
    parser.add_argument(
        "-sm",
        "--scale_mode",
        default=True,
        type=bool,
        help="Scale or crop the video output. Default is scale. Set to false to switch to crop mode",
    )
    args = parser.parse_args()

    pipeline = dai.Pipeline()

    ## Color camera
    COLOR_FPS = 30
    colorCam = pipeline.create(dai.node.ColorCamera)
    colorCam.setResolution(dai.ColorCameraProperties.SensorResolution.THE_4_K)
    colorCam.setInterleaved(False)
    colorCam.setColorOrder(dai.ColorCameraProperties.ColorOrder.BGR)
    colorCam.setFps(COLOR_FPS)
    # Note if this is different from resolution, the OAK scales
    colorCam.setVideoSize(args.width, args.height)
    # if SCALE_ON is True:
    #     colorCam.setIspScale(WIDTH, 1920)

    videnc = pipeline.create(dai.node.VideoEncoder)
    videnc.setDefaultProfilePreset(
        COLOR_FPS, dai.VideoEncoderProperties.Profile.H265_MAIN
    )
    videnc.setKeyframeFrequency(COLOR_FPS * 4)
    videnc.setQuality(args.quality)

    colorCam.video.link(videnc.input)

    videncOut = pipeline.create(dai.node.XLinkOut)
    videncOut.setStreamName("color_encoded")
    videnc.bitstream.link(videncOut.input)

    #
    # Set up B&W cameras for raw output
    #
    MONO_FPS = 10
    monoLeft = pipeline.create(dai.node.MonoCamera)
    monoLeft.setResolution(dai.MonoCameraProperties.SensorResolution.THE_800_P)
    monoLeft.setFps(MONO_FPS)
    xoutLeft = pipeline.create(dai.node.XLinkOut)
    xoutLeft.setStreamName("left")
    monoLeft.out.link(xoutLeft.input)

    monoRight = pipeline.create(dai.node.MonoCamera)
    monoRight.setResolution(dai.MonoCameraProperties.SensorResolution.THE_800_P)
    monoRight.setFps(MONO_FPS)
    xoutRight = pipeline.create(dai.node.XLinkOut)
    xoutRight.setStreamName("right")
    monoRight.out.link(xoutRight.input)

    at_detector = ApriltagDetector()

    #
    # Select which device to use
    #
    device_infos = dai.Device.getAllAvailableDevices()
    if len(device_infos) == 0:
        raise RuntimeError("No DepthAI device found!")
    else:
        print("Available devices:")
        for i, info in enumerate(device_infos):
            print(f"[{i}] {info.getMxId()} [{info.state.name}]")

        if len(device_infos) == 1:
            device_info = device_infos[0]
        else:
            parser.error("Can't handle more than one OAK device")

    if device_info.protocol != dai.XLinkProtocol.X_LINK_USB_VSC:
        print(
            f"Running RTSP stream may be unstable due to connection... (protocol: {device_info.protocol})"
        )

    ############################

    streamer = RtspStreamer(args.rtsp_host, args.rtsp_port)

    color_ffmpeg = streamer.color_streamer(COLOR_FPS)

    left_ffmpeg = streamer.mono_streamer("left", resolution="1280x800", pi_hw=True)
    #right_ffmpeg = streamer.mono_streamer("right", resolution="1280x800", pi_hw=True)

    with dai.Device(pipeline) as device:
        print(f"USB Speed: {device.getUsbSpeed()}")

        ## Load calibration data
        calibration = device.readCalibration()

        left_detector = at_detector.dai_datector(
            calibration.getCameraIntrinsics(calibration.getStereoLeftCameraId())
        )

        right_detector = at_detector.dai_datector(
            calibration.getCameraIntrinsics(calibration.getStereoRightCameraId())
        )

        color_encoded = device.getOutputQueue(
            "color_encoded", maxSize=40, blocking=False
        )
        left_stream = device.getOutputQueue("left", maxSize=4, blocking=False)
        right_stream = device.getOutputQueue("right", maxSize=4, blocking=False)

        try:
            while True:
                # while color_encoded.has():
                #     data = color_encoded.get().getData()
                #     color_ffmpeg.stdin.write(data)

                while left_stream.has():
                    img = left_stream.get().getCvFrame()
                    #tags = left_detector.detect(img)
                    #print(f"Left: found {len(tags)} tags")

                    #color_img = left_detector.draw_tags(img, tags)
                    color_img = img
                    left_ffmpeg.stdin.write(color_img.astype(np.uint8).tobytes())

                # while right_stream.has():
                #     img = right_stream.get().getCvFrame()
                #     #tags = right_detector.detect(img)
                #     right_ffmpeg.stdin.write(img.astype(np.uint8).tobytes())

                    #print(f"Right: found {len(tags)} tags")

        except KeyboardInterrupt:
            pass

        except Exception as exc:
            print(f"Exception: {exc}")
            pass

        # proc.stdin.close()
