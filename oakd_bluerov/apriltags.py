from pyapriltags import Detector
import cv2


class ApriltagDetector:
    def __init__(self, nthreads=4, quad_decimate=2.0):
        self.detector = Detector(
            families="tag36h11",
            nthreads=nthreads,
            quad_decimate=quad_decimate,
            quad_sigma=0.0,
            refine_edges=1,
            decode_sharpening=0.25,
            debug=0,
        )

    def dai_datector(self, intrinsics):
        return DepthAIApriltagDetector(self.detector, intrinsics)


class DepthAIApriltagDetector:
    def __init__(self, detector, intrinsics):
        self.detector = detector

        # pyapriltags uses a 4-vector of [fx, fy, cx, cy]
        self.at_params = [
            intrinsics[0][0],
            intrinsics[1][1],
            intrinsics[0][2],
            intrinsics[1][2],
        ]

    def detect(self, img):
        return self.detector.detect(
            img,
            estimate_tag_pose=False,
            camera_params=self.at_params,
            tag_size=None,
        )

    def draw_tags(self, img, tags):
        color_img = cv2.cvtColor(img, cv2.COLOR_GRAY2RGB)

        for tag in tags:
            for idx in range(len(tag.corners)):
                cv2.line(
                    color_img,
                    tuple(tag.corners[idx - 1, :].astype(int)),
                    tuple(tag.corners[idx, :].astype(int)),
                    (0, 255, 0),
                    thickness=3,
                )

            cv2.putText(
                color_img,
                str(tag.tag_id),
                org=(
                    tag.corners[0, 0].astype(int) + 10,
                    tag.corners[0, 1].astype(int) + 10,
                ),
                fontFace=cv2.FONT_HERSHEY_SIMPLEX,
                fontScale=0.8,
                thickness=3,
                color=(0, 0, 255),
            )

        return color_img
