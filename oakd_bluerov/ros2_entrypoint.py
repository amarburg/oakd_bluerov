import rclpy
from rclpy.node import Node
import depthai as dai
import numpy as np
from itertools import chain

from std_msgs.msg import String

from sensor_msgs.msg import Image, CameraInfo
from cv_bridge import CvBridge, CvBridgeError

from oakd_bluerov.depthai import Oakd
from oakd_bluerov.apriltags import ApriltagDetector
from oakd_bluerov.rtsp_streams import RtspStreamer


# Takes a DepthAI.CalibrationHandler and a camera id
# Returns a CameraInfo message
# 
# Currently only processes intrinsics.  Conversion copied from:
#  https://github.com/luxonis/depthai-ros/blob/a15039cf468818a7451ce7e9f921c73037be8c3e/depthai_bridge/src/ImageConverter.cpp#L334
# 
def make_camera_info( calib, camera_id, width = None, height = None ):
    cam_info = CameraInfo()

    _, def_width, def_height = calib.getDefaultIntrinsics( camera_id )

    if width is None:
        width = def_width

    if height is None:
        height = def_height

    cam_info.width = width
    cam_info.height = height

    k_2d = calib.getCameraIntrinsics(camera_id, resizeWidth=width, resizeHeight=height)
    cam_info.k = list(chain.from_iterable(k_2d))
    cam_info.d = calib.getDistortionCoefficients(camera_id)
    cam_info.distortion_model = "rational_polynomial"

    return cam_info


class OakdBluerov(Node):
    def __init__(self):
        super().__init__("oakd_bluerov")
        self.publisher_ = self.create_publisher(String, "topic", 10)

        self.declare_parameter("color_fps", 30)
        self.declare_parameter("color_quality", 80)

        self.declare_parameter("mono_fps", 10)
        self.declare_parameter("stream_left", False)

        self.declare_parameter("rtsp/host", "localhost")
        self.declare_parameter("rtsp/port", 8554)

        self.declare_parameter("rpi_h264_hwaccel", False)

        # Configure publishers
        #
        self.bridge = CvBridge()
        self.left_image_pub = self.create_publisher(Image, "/left/image_raw", 10)
        self.left_camera_info_pub = self.create_publisher(CameraInfo, "/left/camera_info", 10)

        self.right_image_pub = self.create_publisher(Image, "/right/image_raw", 10)
        self.right_camera_info_pub = self.create_publisher(
            CameraInfo, "/right/camera_info", 10
        )

    def run(self):
        color_fps = self.get_parameter("color_fps").get_parameter_value().integer_value
        color_quality = (
            self.get_parameter("color_quality").get_parameter_value().integer_value
        )

        mono_fps = self.get_parameter("mono_fps").get_parameter_value().integer_value

        oakd = Oakd()

        self.get_logger().info(f"Color camera: {color_fps} FPS")
        _ = oakd.color_pipeline(fps=color_fps, quality=color_quality)

        self.get_logger().info(f"Mono camera: {mono_fps} FPS")
        _ = oakd.mono_pipeline("left", fps=mono_fps)
        _ = oakd.mono_pipeline("right", fps=mono_fps)

        rpi_accel = self.get_parameter("rpi_h264_hwaccel").get_parameter_value()
        if rpi_accel:
            self.get_logger().info("**ENABLING** Raspi h264 acceleration")

        #
        # Select which device to use
        #
        device_infos = dai.Device.getAllAvailableDevices()
        if len(device_infos) == 0:
            raise RuntimeError("No DepthAI device found!")
        else:
            print("Available devices:")
            for i, info in enumerate(device_infos):
                print(f"[{i}] {info.getMxId()} [{info.state.name}]")

            if len(device_infos) == 1:
                device_info = device_infos[0]
            else:
                pass
                # parser.error("Can't handle more than one OAK device")

        if device_info.protocol != dai.XLinkProtocol.X_LINK_USB_VSC:
            print(
                f"Running RTSP stream may be unstable due to connection... (protocol: {device_info.protocol})"
            )

        ############################
        rtsp_host = self.get_parameter("rtsp/host").get_parameter_value().string_value
        rtsp_port = self.get_parameter("rtsp/port").get_parameter_value().integer_value

        streamer = RtspStreamer(rtsp_host, rtsp_port)

        self.color_ffmpeg = streamer.color_streamer(color_fps)

        self.left_ffmpeg = None
        self.right_ffmpeg = None

        if self.get_parameter("stream_left"):
            self.left_ffmpeg = streamer.mono_streamer("left", "1280x800", pi_hw=rpi_accel)

        self.device = dai.Device(oakd.pipeline, maxUsbSpeed=dai.UsbSpeed.HIGH)

        print(f"USB Speed: {self.device.getUsbSpeed()}")

        ## Load calibration data
        calibration = self.device.readCalibration()

        ## Convert calibration to CameraInfo messages
        self.left_camera_info = make_camera_info(
            calibration, calibration.getStereoLeftCameraId()
        )
        self.right_camera_info = make_camera_info(
            calibration, calibration.getStereoRightCameraId()
        )

        # Connect to output queues
        self.color_encoded_stream = self.device.getOutputQueue(
            "color_encoded", maxSize=40, blocking=False
        )
        self.color_encoded_stream.addCallback( self.color_camera_cb )

        self.left_stream = self.device.getOutputQueue("left", maxSize=4, blocking=False)
        self.left_stream.addCallback( self.left_camera_cb )

    def shutdown(self):
        self.device.close()
        self.color_ffmpeg.close()

        if self.left_ffmpeg is not None:
            self.left_ffmpeg.close()

    def color_camera_cb(self, xlink_msg):
        #self.get_logger().info("In color callback")

        try:
            data = xlink_msg.getData()
            self.color_ffmpeg.write(data)
        except Exception as exc:
            self.get_logger().warn(f"Color exception: {exc}")
            pass

    def left_camera_cb(self, xlink_msg):
        #self.get_logger().info("In callback")

        try:
            if self.left_ffmpeg is not None or \
            self.left_image_pub.get_subscription_count() > 0 or \
            self.left_image_info.get_subscription_count() > 0:
                img = self.left_stream.get().getCvFrame()

                if self.left_ffmpeg is not None:
                    self.left_ffmpeg.write(img.astype(np.uint8).tobytes())

                img_msg = self.bridge.cv2_to_imgmsg(img, "mono8")
                self.left_image_pub.publish(img_msg)

                self.left_camera_info.header = img_msg.header
                self.left_camera_info_pub.publish(self.left_camera_info)
        except Exception as exc:
            self.get_logger().warn(f"Left exception: {exc}")
            pass

    def right_camera_cb(self, xlink_msg):
        try:
            if self.right_image_pub.get_subscription_count() > 0 or \
            self.right_image_info.get_subscription_count() > 0:
                
                img = self.right_stream.get().getCvFrame()

                img_msg = self.bridge.cv2_to_imgmsg(img, "mono8")
                self.right_image_pub.publish(img_msg)

                self.right_camera_info.header = img_msg.header
                self.right_camera_info_pub.publish(self.right_camera_info)
        except Exception:
            pass

        # try:
        #     while self.right_stream.has():
        #         img = self.right_stream.get().getCvFrame()
        #         tags = self.right_detector.detect(img)
        #         self.right_ffmpeg.write(img.astype(np.uint8).tobytes())

        #     #print(f"Right: found {len(tags)} tags")
        # except Exception:
        #     # print(f"Right xception: {exc}")
        #     pass


def entrypoint(args=None):
    rclpy.init(args=args)

    oakd_bluerov = OakdBluerov()

    oakd_bluerov.run()

    try:
        rclpy.spin(oakd_bluerov)
    except KeyboardInterrupt:
        oakd_bluerov.shutdown()


if __name__ == "__main__":
    entrypoint()
