# Unified OAK-D ROS2 driver for BlueROV

This is a monolithic ROS2 driver for the OAK-D camera on the BlueROV.   Why monolithic?  We generally don't have the CPU or network bandwidth to pass around full image streams, this encapsulates all of the critical vision functions into a single context; with only derived ROS2 products (e.g. Apriltag detections) published.
